var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET users listing. */
router.get('/', function(req, res, next) {
    if (process.env.NODE_ENV == 'development'){
        fs.readFile("recursos/data/data.json", "utf8", function(err, data){
            if(err) throw err;
            res.send(data);
        });
    }else{
        //Implement this for PROD use
    }
});
router.post('/', function(req, res) {
    //Check if request is JSON
    if (!req.is('*/json'))
        res.status(400).sendStatus({ error: 'Request is not json' })

    try {
        hotel = JSON.parse(JSON.stringify(req.body))
    } catch (error) {
        //JSON is not valid
        res.status(400).sendStatus({ error: 'Invalid fields. Check your JSON data.' })
    }


    //Assume all fields required
    var id = hotel.id;
    var name = hotel.name;
    var stars = hotel.stars;
    var price = hotel.price;
    var image = hotel.image;
    var amenities = hotel.amenities


    if (id && name && stars && price && image && amenities) {
        //Should persist hotel somewhere and return the id of hotel persisted
        res.send(hotel);
    }else{
        res.status(400).sendStatus({ error: 'Malformed request. Maybe you forget a required param' })
    }
});
router.put('/:id',function(req, res){

});
router.delete('/:id',function(req, res){

});
router.get('/search', function (req, res, next) {
    if (process.env.NODE_ENV == 'development'){
        var hotels_json = JSON.parse(fs.readFileSync("recursos/data/data.json", "utf8"));
        var query = {};

        var hotels_json_filtered = hotels_json.filter(function(el){
            var isOn = true;
            Object.keys(req.query).forEach(function(key, value) {
                isOn = el[key] == req.query[key];
            });
            return isOn;
        });
        res.send(hotels_json_filtered);
    }else{
        //Implement this for PROD use
    }
})
module.exports = router;
