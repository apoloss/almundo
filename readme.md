# Almundo test

Este es una evaluacion de perfiles frontend para Almundo.com

## Empezando

El proyecto se encuentra dividido en dos sub-proyectos. El primero se encuentra en la carpeta __api/__ y el segundo en la carpeta __front/__.

La API fue desarrollada utilizando [express](http://expressjs.com/) como servidor. El front por el otro lado fue desarrollado en [Vue.js](https://vuejs.org/), para este desarrollo se han creado 3 components:

* AlmundoHeader
* Hotel
* HotelFilter

La comunicacion entre el component Hotel y HotelFilter esta intervenida por un [EventBus](https://vuejs.org/v2/guide/components.html#Non-Parent-Child-Communication).


### Prerequisitos

node/npm son programas necesarios para la instalación del/los proyecto/proyectos

### Estructura del proyecto

```
almundo
└───api
│   │   app.js
│   │   package.json
│   │
│   │───bin
|   │───routes
│   └───recursos
│
└───front
```

### Instalación de ambiente dev



```
1. npm install en directorio api/
2. npm run dev en directorio api/
3. npm install en directorio front/
4. npm run dev en directorio front/
```

En este punto deberias tener __corriendo la API en el puerto 3000__ de localhost __y webpack en modo dev (webpack-dev-server) en el puerto 8080__.


#### NODE_ENV

La API maneja dos ambientes (development & production) mediante la variable de entorno __NODE_ENV__. A fines de poder testear la API sin problemas, se ha tenido el cuenta los headers CORS __solamente en development__.

__Las opciones para listar y buscar hoteles son tomadas del archivo data.json que se encuentra en la carpeta recursos/ (solo en development)__

#### Producción
Para exportar el front a produccion se debe correr el comando:

 - `npm run build`

El output de este comando es una carpeta dist/ en donde se encuentran todos los archivos que se deberian copiar en el webserver.


#### Decisiones tomadas para el proyecto
En el primer punto del punto 1 del test dice:

> La funcionalidad de listado y filtrado de hoteles debe estar soportada por la API y
consumida en la aplicación cliente.

Si bien la funcionalidad de search esta soportada por la API, tome la decisión de no consumirla cada vez que se busca filtrar un hotel (el mejor request es el que no se hace). Para esto se utilizaron los mismos estados de los componentes.

Las transiciones tambien fueron asumidas, ya que en el test, no encontre algo especifico.
